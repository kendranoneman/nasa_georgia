import matplotlib.pyplot as plt
import numpy as np
import sys
import sympy as sp
import math
from itertools import combinations
from scipy.spatial.distance import squareform, pdist

bohr = 0.529177 #angstrom
pi = 3.14159265359

FILE = sys.argv[1]

#Separated rows out due to uneven column counts
x = np.genfromtxt(FILE, delimiter = None, dtype = float, skip_header = 2, usecols = 1)
y = np.genfromtxt(FILE, delimiter = None, dtype = float, skip_header = 2, usecols = 2)
z = np.genfromtxt(FILE, delimiter = None, dtype = float, skip_header = 2, usecols = 3)

#Combined into one big array with all coordinates 
stacked_matrix = np.column_stack((x,y,z))

dist = pdist(stacked_matrix, 'euclidean') #bohr units
r_ij = dist*bohr
print(r_ij)

x_diff = [abs(i - j) for i,j in combinations(x, 2)]
xdiff = np.asarray(x_diff, dtype = float)
y_diff = [abs(a - b) for a,b in combinations(y, 2)]
ydiff = np.asarray(y_diff, dtype = float)
z_diff = [abs(l - m) for l,m in combinations(z, 2)]
zdiff = np.asarray(z_diff, dtype = float)

e_x = np.divide(xdiff,r_ij)*-1
e_y = np.divide(ydiff,r_ij)*-1
e_z = np.divide(zdiff,r_ij)*-1

#e = r_ij[0]*r_ij[1]
'''
r_12 = r_ij[4]
r_13 = r_ij[0]
r_23 = r_ij[1]

cosa = ((r_12**2)-(r_13**2)-(r_23**2))/(-2*r_13*r_23)
angle = math.acos(cosa)
ang = angle*(180/pi)
print(ang)
'''

    
'''
angle = math.cos(e)
ang = angle*(180/pi)+90
print(ang)

e31x = x[0]*x[1]
e31y = y[0]*y[1]
e31z = z[0]*z[1]
e31 = e31x + e31y + e31z
print(e31)
e32x = x[0]*x[2]
e32y = y[0]*y[2]
e32z = z[0]*z[2]
e32 = e32x + e32y + e32z
e = e31*e32
print(e)
angle = np.arccos(e)
pi = 3.14159265359
ang = angle*(180/pi)
#print(ang)
'''
'''
pi = 3.14159265359
cosa = e_x*e_y*e_z
angle = np.arccos(cosa)
ang = angle*(180/pi)
#print(ang)
'''
